import Data.Char

-- 1. Implement the `map` function using a fold.
map' :: (a -> b) -> [a] -> [b]
map' f = foldr (fun f) []
  where
    fun f elt acc = (f elt):acc

-- 2. Implement the `filter` function using a fold.
filter' :: (a -> Bool) -> [a] -> [a]
filter' pred = foldr (decider pred) []
  where
    decider pred elt acc =
      if pred elt then elt:acc else acc

-- 4. Smallest positive number evenly divisble by n
-- spnedbn 20 = 232792560
spnedbn :: Int -> Int
spnedbn n =
  head candidates -- Get the first of the numbers that satisfy the condition
  where
    naturals = [1..]
    pred x = all (\y -> x `rem` y == 0) [1..n]
    candidates = filter pred naturals

main = do
  runTests
  print $ spnedbn 20

-- Run correctness tests
runTests :: IO ()
runTests =
  let naturals = take 18 [1..]
      strings  = words "The Glorious Glasgow Haskell Compilation System, version 8.6.3"
  in do
    assertEqual (map (+1) naturals) (map' (+1) naturals)
    assertEqual (map reverse strings) (map' reverse strings)
    assertEqual (filter even naturals) (filter' even naturals)
    assertEqual (filter (all isAlpha) strings) (filter' (all isAlpha) strings)

-- Evaluating this function returns an empty IO action type if the two values
-- are equal, but prints a warning if they are not
assertEqual :: (Eq a, Show a) => a -> a -> IO ()
assertEqual one two =
  if one == two
    then pure ()
    else putStrLn $ "Assertion fail: " ++ show one ++ " /= "  ++ show two
